# Team Progress / Grundschule | Projekt Idee - Geschäftsmodell - MVP: Software as a Service

This projekt is a students bachelor project for the module 'Projekt Idee - Geschäftsmodell - MVP: Software as a Service' at the university of Hamburg, Germany. <br />
Progress is a platform for teachers and researchers for the documentation of lessons and also to increase the quality of the lessons. <br />
We support the educational research by providing first class data directly from the teachers. 

This is the repository for the backend of our project.

## Setting up a local development environment
### Requirements

- Java 11
- Docker and docker-compose
    - macOS: `brew cask install Docker`
    - windows: [How to install docker on Windows](https://docs.docker.com/docker-for-windows/install/)  
    - Linux: install via package-manager (apt, yum, pacman, ...)
- create a docker volume called "backend-postgres"
    - `docker volume create backend-postgres`

We use Intellij as our IDE

### Default Setup
1. [How to import a project in IntelliJ](https://confluence.ppi.int/display/PPIX/How+to+import+a+project+in+IntelliJ)
2. Once Intellij is up and running, right click on `build.gradle.kts` and choose "Import Gradle project"

### Alternative Setup
1. [Install Postgres](https://www.google.de/search?q=how+to+install+postgresql)
2. Run all commands mentioned in [init script](./docker/postgres/initUser.sql) in postgres database
3. Run application with local spring profile or use "LocalApplication" Intellij run config 

### Run Project
* `./gradlew run` in Terminal  
or
* run the "run" goal in IntelliJ

This will start two docker containers, one for the database (postgresql)
and one with spring boot.  
You can access the backend at http://localhost:8080.

## Additional Info
- If you alter existing database tables, you'll have to delete the docker volume (assuming you use docker)
  and create a new one.
