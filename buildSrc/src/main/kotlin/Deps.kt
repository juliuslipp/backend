object Deps {

    //Plugins
    const val plugin_spring_boot_version = "2.4.1"
    const val plugin_spring_deps_management_version = "1.0.10.RELEASE"


    // Persistance
    const val postgres_version = "42.1.4"
    const val hibernate_core_version = "5.4.27.Final"
    const val hibernate_validator_version = "7.0.0.Final"
    const val zaxxer_HikariCP_version = "3.4.5"
    const val flywaydb_core_version = "7.3.2"

    // security
    const val java_jwt_version = "3.12.0"

    //etc
    const val lombok_version = "1.18.16"
    const val modelmapper_version = "2.3.9"
    const val swagger_version = "2.9.2"
    const val dockercompose_plugin_version = "0.14.0"
    const val logstash_logback_version = "4.11"
    const val retrofit2_version = "2.9.0"
    const val slf4j_version = "1.7.25"



}