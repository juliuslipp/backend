import org.gradle.plugins.ide.idea.model.IdeaModel


group = "com.uhh.grundschule"
version = "0.0.1"

plugins {
    java
    id("idea")
    id("org.springframework.boot") version Deps.plugin_spring_boot_version
    id("io.spring.dependency-management") version Deps.plugin_spring_deps_management_version
    id("com.avast.gradle.docker-compose") version Deps.dockercompose_plugin_version
}

configure<IdeaModel> {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

dependencies {
    // spring
    implementation( group = "org.springframework.boot", name = "spring-boot-starter-data-jpa")
    implementation( group = "org.springframework.boot", name = "spring-boot-starter-security")
    implementation( group = "org.springframework.boot", name = "spring-boot-starter-web")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")

    // security
    implementation(group = "com.auth0", name = "java-jwt", version = Deps.java_jwt_version)

    // persistence
    implementation(group = "org.postgresql", name = "postgresql", version = Deps.postgres_version)
    implementation(group = "org.hibernate", name = "hibernate-core", version = Deps.hibernate_core_version)
    implementation(group = "org.hibernate", name = "hibernate-validator", version = Deps.hibernate_validator_version)
    // implementation(group = "org.flywaydb", name = "flyway-core", version = Deps.flywaydb_core_version)

    implementation(group = "org.springdoc", name = "springdoc-openapi-ui", version = "1.5.2")

    // lombok
    implementation(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    testImplementation(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    annotationProcessor(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    testAnnotationProcessor(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)

    // utils
    implementation(group = "org.modelmapper", name = "modelmapper", version = Deps.modelmapper_version)
    implementation(group = "net.logstash.logback", name = "logstash-logback-encoder", version = Deps.logstash_logback_version)
    implementation(group = "com.squareup.retrofit2", name = "retrofit", version = Deps.retrofit2_version)
    implementation(group = "org.slf4j", name = "slf4j-api", version = Deps.slf4j_version)
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
}

configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

configure<IdeaModel> {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

tasks.register("run") {
    dependsOn(composeUp)
}

val composeUp by tasks.existing {
    dependsOn("assemble")
}

tasks {
    // Use the built-in JUnit support of Gradle.
    "test"(Test::class) {
        useJUnitPlatform()
    }
}

dockerCompose {

    useComposeFiles = listOf("docker/docker-compose.yml")
    buildBeforeUp = true
    ignorePullFailure = false
    ignorePushFailure = false
    buildAdditionalArgs = listOf("--force-rm")
    pushServices = listOf()
    pullAdditionalArgs = listOf("--ignore-pull-failures")
    upAdditionalArgs = listOf("--no-deps")
    downAdditionalArgs = listOf()
    waitForTcpPorts = true
    captureContainersOutput = false
    containerLogToDir = project.file("logs")
    composeLogToFile = project.file("${buildDir}/compose-logs.txt")
    stopContainers = true
    removeContainers = true
    removeOrphans = true
    projectName = "progress"
}

