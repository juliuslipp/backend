package com.uhh.grundschule.backend.business.service.api;

import com.uhh.grundschule.backend.api.v1.dto.UserDto;
import com.uhh.grundschule.backend.api.v1.dto.UserInfoDto;
import com.uhh.grundschule.backend.common.model.User;
import javax.transaction.Transactional;

public interface UserService {

  User create(UserDto userDto);

  User getUserInfo();

  User setUserInfo(User user);

  boolean setUserPassword(String oldPassword, String newPassword);
}
