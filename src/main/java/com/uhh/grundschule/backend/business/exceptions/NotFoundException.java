package com.uhh.grundschule.backend.business.exceptions;

import lombok.Getter;
import lombok.NonNull;

import java.util.Optional;
import java.util.UUID;

/**
 * Exception will be thrown when a object was not found.
 */
@Getter
public class NotFoundException extends RuntimeException {

    private static final String NULL_ID = "null_id";
    private final String searchTerm;
    private final Class type;

    /**
     * Throw if a object of a given was not found.
     *
     * @param id   expected Id
     * @param type Type of searched object
     * @param <T>  ClassType
     */
    public <T> NotFoundException(final Long id, @NonNull final Class<T> type) {
        this(Optional.ofNullable(id).map(Object::toString).orElse(NULL_ID), type);
    }

    public <T> NotFoundException(@NonNull final String term, @NonNull final Class<T> type) {
        this.searchTerm = term;
        this.type = type;
    }


    @Override
    public String getMessage() {
        return String.format("{%s} cannot be found with term: '%s'", this.type.getSimpleName(), this.searchTerm);
    }
}
