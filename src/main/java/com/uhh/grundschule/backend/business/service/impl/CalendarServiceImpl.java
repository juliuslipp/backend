package com.uhh.grundschule.backend.business.service.impl;

import com.uhh.grundschule.backend.business.service.api.CalendarService;
import com.uhh.grundschule.backend.common.model.User;
import com.uhh.grundschule.backend.common.model.Calendar;
import com.uhh.grundschule.backend.persistence.dao.UserDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CalendarServiceImpl implements CalendarService {

    private final UserServiceImpl userService;
    private final UserDao userDao;

    @Override
    public void setAppointments(String appointmentData) {
        User user = this.userService.getCurrentUser();
        user.getCalendar().setAppointmentData(appointmentData);
        userDao.save(user);
    }

    @Override
    public void reset() {
        User user = userService.getCurrentUser();
        user.getCalendar().setAppointmentData("");
        userDao.save(user);
    }

    @Override
    public Calendar get() {
        User user = userService.getCurrentUser();
        return user.getCalendar();
    }

    @Override
    public void set(Calendar calendar) {
        User user = userService.getCurrentUser();
        user.setCalendar(calendar);
        userDao.save(user);
    }
}
