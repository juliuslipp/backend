package com.uhh.grundschule.backend.business.service.impl;

import com.uhh.grundschule.backend.api.v1.dto.document.DocumentDto;
import com.uhh.grundschule.backend.business.exceptions.NotFoundException;
import com.uhh.grundschule.backend.business.service.api.DocumentService;
import com.uhh.grundschule.backend.common.annotations.LogMethod;
import com.uhh.grundschule.backend.common.model.teaching.document.*;
import com.uhh.grundschule.backend.persistence.dao.*;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private final DocumentDao documentDao;
    private final MaterialDao materialDao;
    private final MaterialDataDao materialDataDao;
    private final TeachingPhaseDao teachingPhaseDao;
    private final CommentDao commentDao;

    private final ModelMapper modelMapper;


    private void saveMaterial(Material material) {
        for (MaterialData data : material.getData()) {
            data.setCreatedAt(LocalDateTime.now());
            data.setUpdatedAt(LocalDateTime.now());
            this.materialDataDao.save(data);
        }
        this.materialDao.save(material);
    }

    @Override
    @Transactional
    @LogMethod
    public Long create(DocumentDto dto) {
        Document doc = this.modelMapper.map(dto, Document.class);
        for (Comment comment : doc.getCommentList()) this.commentDao.save(comment);
        for (Material m : doc.getMaterialList()) saveMaterial(m);
        for (TeachingPhase tp : doc.getTeachingPhaseList()) {
            this.saveMaterial(tp.getMaterial());
            this.teachingPhaseDao.save(tp);
        }
        this.documentDao.save(doc);
        return doc.getId();
    }

    @Override
    @Transactional
    @LogMethod
    public void overwrite(Long documentId, DocumentDto dto) {
        Document newDoc = this.get(documentId);
        modelMapper.map(dto, newDoc);
        documentDao.save(newDoc);
    }

    @Override
    public Document get(Long documentId) {
        return this.documentDao.findById(documentId)
                .orElseThrow(() -> new NotFoundException(documentId, Document.class));
    }

    @Override
    public List<Document> getList() {
        return this.documentDao.findAll();
    }

    @Override
    public List<Document> getListByUser() {
        return getList();
    }

    @Override
    public List<Material> getMaterialList(Long documentId) {
        return this.get(documentId).getMaterialList();
    }

    @Override
    public Document getInformation(Long documentId) {
        Document document = this.modelMapper.map(this.get(documentId), Document.class);
        document.setMaterialList(null);
        return document;
    }

    @Override
    public List<DocumentMetaData> getListMetaData() {
        return this.documentDao.findAllMetaData();
    }

    @Override
    public void delete(Long documentId) {
        this.documentDao.deleteById(documentId);
    }
}
