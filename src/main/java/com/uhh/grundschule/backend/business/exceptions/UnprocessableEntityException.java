package com.uhh.grundschule.backend.business.exceptions;

/**
 * Exception will be thrown when an entity cannot be processed.
 */
public class UnprocessableEntityException extends RuntimeException {

    public UnprocessableEntityException(String message) {
        super(message);
    }
}
