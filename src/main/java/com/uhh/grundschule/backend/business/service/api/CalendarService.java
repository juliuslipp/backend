package com.uhh.grundschule.backend.business.service.api;

import com.uhh.grundschule.backend.common.model.Calendar;

public interface CalendarService {

    void setAppointments(String appointmentData);

    void reset();

    Calendar get();

    void set(Calendar calendar);
}
