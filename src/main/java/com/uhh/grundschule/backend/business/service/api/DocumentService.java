package com.uhh.grundschule.backend.business.service.api;

import com.uhh.grundschule.backend.api.v1.dto.document.DocumentDto;
import com.uhh.grundschule.backend.common.model.teaching.document.Document;
import com.uhh.grundschule.backend.common.model.teaching.document.DocumentMetaData;
import com.uhh.grundschule.backend.common.model.teaching.document.Material;

import java.util.Arrays;
import java.util.List;

public interface DocumentService {

    Long create(DocumentDto dto);

    void overwrite(Long documentId, DocumentDto dto);

    Document get(Long documentId);

    List<Document> getList();

    List<Document> getListByUser();

    List<Material> getMaterialList(Long documentId);

    Document getInformation(Long documentId);

    List<DocumentMetaData> getListMetaData();

    void delete(Long documentId);
}
