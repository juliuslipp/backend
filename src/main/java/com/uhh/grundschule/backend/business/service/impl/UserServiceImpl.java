package com.uhh.grundschule.backend.business.service.impl;

import com.uhh.grundschule.backend.api.v1.dto.UserDto;
import com.uhh.grundschule.backend.business.service.api.UserService;
import com.uhh.grundschule.backend.common.annotations.LogMethod;
import com.uhh.grundschule.backend.common.enums.UserType;
import com.uhh.grundschule.backend.common.model.User;
import com.uhh.grundschule.backend.common.model.Calendar;
import com.uhh.grundschule.backend.persistence.dao.UserDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ModelMapper modelMapper;
    private final UserDao userDao;

    @Transactional
    @Override
    @LogMethod
    public User create(UserDto userDto) {
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        User user = modelMapper.map(userDto, User.class);
        if (user.getUserType() == null) user.setUserType(UserType.TEACHER);
        user.setCalendar(new Calendar());
        return userDao.save(user);
    }

    @Override
    public User getUserInfo() {
        return getCurrentUser();
    }

    @Transactional
    @Override
    @LogMethod
    public User setUserInfo(User user) {
        User newUser = this.getCurrentUser();
        modelMapper.map(user, newUser);
        userDao.save(newUser);
        return newUser;
    }

    @Transactional
    @Override
    @LogMethod
    public boolean setUserPassword(String oldPassword, String newPassword) {
        User user = getCurrentUser();
        if (bCryptPasswordEncoder.matches(oldPassword, user.getPassword())) {
            userDao.setPassword(user.getId(),
                    bCryptPasswordEncoder.encode(newPassword));
            return true;
        }
        return false;
    }

    public User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email;

        if (principal instanceof UserDetails)
            email = ((UserDetails) principal).getUsername();
        else if (principal != null)
            email = principal.toString();
        else
            email = null;

        return userDao.findByEmail(email);
    }

}

