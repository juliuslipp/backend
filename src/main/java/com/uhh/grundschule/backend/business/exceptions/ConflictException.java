package com.uhh.grundschule.backend.business.exceptions;

/**
 * Exception will be thrown when a requested operation caused a conflict.
 */
public class ConflictException extends RuntimeException {
    public ConflictException(final String message) {
        super(message);
    }
}