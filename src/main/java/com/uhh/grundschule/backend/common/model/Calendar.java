package com.uhh.grundschule.backend.common.model;

import com.uhh.grundschule.backend.common.model.User;
import lombok.*;

import javax.persistence.*;
import javax.transaction.Transactional;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Calendar {

    @Column(nullable = false)
    private Integer cellDuration = 60;

    @Column(nullable = false)
    private Boolean showCurrentTime = true;

    @Column(nullable = false)
    private Integer startDayHour = 6;

    @Column(nullable = false)
    private Integer endDayHour = 20;

    @Column(columnDefinition="TEXT")
    private String appointmentData;

}
