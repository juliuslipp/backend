package com.uhh.grundschule.backend.common.security;

public class SecurityConstants {

    public static final String SECRET = "abc";
    public static final long EXPIRATION_TIME = 900_000_000; // weeks mins
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/v1/user";
    public static final String[] AUTH_WHITELIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
    };
}
