package com.uhh.grundschule.backend.common.model.moderating;

import com.uhh.grundschule.backend.common.model.User;
import java.util.List;
import javax.persistence.*;

import com.uhh.grundschule.backend.common.model.teaching.Education;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
@Setter
@Getter
public class Moderator extends User {

}
