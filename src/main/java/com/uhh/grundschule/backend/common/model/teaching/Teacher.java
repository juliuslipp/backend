package com.uhh.grundschule.backend.common.model.teaching;

import com.uhh.grundschule.backend.common.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@DiscriminatorValue("teacher")
@Table
public class Teacher extends User {

    @OneToMany(mappedBy = "teacher")
    private List<Education> educationList;

}
