package com.uhh.grundschule.backend.common.enums;

public enum UserType {
  TEACHER,
  SCIENTIST,
  ADMIN,
}

