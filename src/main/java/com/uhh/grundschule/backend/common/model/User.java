package com.uhh.grundschule.backend.common.model;

import com.uhh.grundschule.backend.common.enums.UserType;
import com.uhh.grundschule.backend.common.model.embeddable.Address;
import com.uhh.grundschule.backend.common.model.teaching.document.Document;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user_account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class User {

    @Id
    @GeneratedValue
    @Setter(AccessLevel.NONE)
    private Long id;

    @Email
    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @Embedded()
    private Address address;

    @Embedded()
    private Calendar calendar;

    @Pattern(regexp = "(^$|[0-9]{10})")
    private String phoneNumber;

    private String organizationName;

    @Enumerated(EnumType.ORDINAL)
    private UserType userType;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "user_documents",
            joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name = "document_id")}
    )
    private Set<Document> documents;

}
