package com.uhh.grundschule.backend.common.model.teaching.document;

import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "comment")
public class Comment extends Model {

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String comment;
}