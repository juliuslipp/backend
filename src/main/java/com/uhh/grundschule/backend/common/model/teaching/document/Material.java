package com.uhh.grundschule.backend.common.model.teaching.document;

import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "material")
@Setter
@Getter
public class Material extends Model {

    @Column
    private String description;

    @OneToMany
    @JoinColumn(name = "material_data_id")
    private List<MaterialData> data;

}