package com.uhh.grundschule.backend.common.enums;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
public class UserRole implements GrantedAuthority {

  private final UserType type;

  @Override
  public String getAuthority() {
    return type.name();
  }
}
