package com.uhh.grundschule.backend.common.model.teaching.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "material_data")
@Setter
@Getter
public class MaterialData extends Model {

    @Column(nullable = false)
    private Byte[] data;

    @JsonIgnore
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @Column(nullable = false)
    private LocalDateTime updatedAt;

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    private String fileType;
}
