package com.uhh.grundschule.backend.common.model.embeddable;

import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Embeddable
@Setter
@Getter
public class Address {

  @NonNull
  private String state;

  @NonNull
  private String city;

  @NonNull
  private String postalCode;

  @NonNull
  private String street;

  @NonNull
  private String streetAdditive;
}
