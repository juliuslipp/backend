package com.uhh.grundschule.backend.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LearningPhase {
    INTRODUCTION,
    GUIDING,
    ELABORATION,
    SECURING;

    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
