package com.uhh.grundschule.backend.common.model.teaching.document;

import com.uhh.grundschule.backend.common.model.User;
import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "document")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Document extends Model {

    @Column(nullable = false, name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;

    @Column(nullable = false, name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(nullable = false)
    private String topic;

    @Column(nullable = false)
    private String textbook;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private String subject;

    @Column
    private String school;

    @Column(nullable = false, name = "school_class")
    private String schoolClass;

    @ManyToMany(mappedBy = "documents")
    private Set<User> users;

    @OneToMany
    @JoinColumn(name = "comments_id")
    private List<Comment> commentList;

    @OneToMany
    @JoinColumn(name = "material_id")
    private List<Material> materialList;


    @OneToMany
    @JoinColumn(name = "teaching_phase_id")
    private List<TeachingPhase> teachingPhaseList;

}