package com.uhh.grundschule.backend.common.model.teaching;

import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "EDUCATION")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Education extends Model {

    @ManyToOne
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;

    @Column(nullable = false)
    private String facility;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String degree;

    private Date startDate;

    private Date endDate;

}
