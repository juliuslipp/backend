package com.uhh.grundschule.backend.common.model.teaching.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class DocumentMetaData {
    private Date startTime;
    private Date endTime;
    private Date date;
    private String topic;
    private String textbook;
    private String state;
    private String subject;
    private String schoolClass;
    private String school;
    private Long id;
}
