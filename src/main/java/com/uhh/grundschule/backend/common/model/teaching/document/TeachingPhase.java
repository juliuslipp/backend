package com.uhh.grundschule.backend.common.model.teaching.document;

import com.uhh.grundschule.backend.common.enums.LearningPhase;
import com.uhh.grundschule.backend.common.model.api.Model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "teaching_phase")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TeachingPhase extends Model {

    @Column(nullable = false, name = "learning_phase")
    @Enumerated(EnumType.ORDINAL)
    private LearningPhase learningPhase;

    @Column(nullable = false)
    private String duration;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String tasks;

    @Column
    private String positiveNotes;

    @Column
    private String problems;

    @ManyToOne
    @JoinColumn(name = "document_id")
    private Document document;

    @OneToOne
    @JoinColumn(name = "material_id")
    private Material material;

}