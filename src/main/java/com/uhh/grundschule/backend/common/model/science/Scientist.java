package com.uhh.grundschule.backend.common.model.science;

import com.uhh.grundschule.backend.common.model.User;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("scientist")
public class Scientist extends User {

}
