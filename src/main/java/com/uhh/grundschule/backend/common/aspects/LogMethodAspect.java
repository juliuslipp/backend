package com.uhh.grundschule.backend.common.aspects;

import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.marker.Markers;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Aspect
@Component
@Slf4j
public class LogMethodAspect {

    public LogMethodAspect() {
        log.info("LogMethod Aspect instantiated");
    }

    @Around("@annotation(com.uhh.grundschule.backend.common.annotations.LogMethod)")
    public Object logMethodExecution(final ProceedingJoinPoint joinPoint) throws Throwable {

        final long startingTime = System.currentTimeMillis();
        final Object returnedValue;

        try {
            returnedValue = joinPoint.proceed();
        } catch (final Exception e) {
            this.logger(joinPoint.getSignature().toShortString(), System.currentTimeMillis() - startingTime, false, joinPoint.getArgs());
            throw e;
        }
        this.logger(joinPoint.getSignature().toShortString(), System.currentTimeMillis() - startingTime, true, joinPoint.getArgs());

        return returnedValue;
    }

    private void logger(final String methodName,
                        final long executionTimeMs,
                        final boolean successful,
                        final Object... additionalData) {

        final Map<String, Object> markerMap = new HashMap<>();


        markerMap.put("EXECUTION_TIME_MS", executionTimeMs);
        markerMap.put("EXCEPTIONALLY", !successful);
        markerMap.put("ARGUMENTS", Arrays.stream(additionalData)
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.joining("\n", "\"", "\"")));


        LogMethodAspect.log.info(Markers.appendEntries(markerMap), "Method '{}' was used.", methodName);
    }

}

