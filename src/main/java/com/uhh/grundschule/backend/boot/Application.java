package com.uhh.grundschule.backend.boot;

import com.uhh.grundschule.backend.api.v1.dto.document.MaterialDataDto;
import com.uhh.grundschule.backend.common.model.teaching.document.MaterialData;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EntityScan("com.uhh.grundschule.backend.common.model")
@EnableJpaRepositories("com.uhh.grundschule.backend.persistence.dao")
@SpringBootApplication(scanBasePackages = {"com.uhh.grundschule.backend"})
public class Application {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.typeMap(MaterialDataDto.class, MaterialData.class)
                .addMapping(MaterialDataDto::getByteData, MaterialData::setData);
        return modelMapper;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
