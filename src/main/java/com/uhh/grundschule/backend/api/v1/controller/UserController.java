package com.uhh.grundschule.backend.api.v1.controller;


import com.uhh.grundschule.backend.api.v1.dto.ScientistInfoDto;
import com.uhh.grundschule.backend.api.v1.dto.TeacherInfoDto;
import com.uhh.grundschule.backend.api.v1.dto.UserDto;
import com.uhh.grundschule.backend.api.v1.dto.UserInfoDto;
import com.uhh.grundschule.backend.business.service.api.UserService;
import com.uhh.grundschule.backend.common.enums.UserType;
import com.uhh.grundschule.backend.common.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/user")
@AllArgsConstructor(onConstructor_ = @Autowired)
@ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "403", description = "User not authenticated", content = @Content(schema = @Schema())),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema()))
})
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    /**
     * @param userDto A user DTO container user data.
     * @return Email of the newly created user.
     */
    @PostMapping
    @Operation(summary = "Creates a new User", description = "Creates a new User and returns the email of the newly created user.")
    @ApiResponse(responseCode = "200", description = "User was created successfully.")
    @ApiResponse(responseCode = "500", description = "User already exists")
    public String create(@Valid @RequestBody UserDto userDto) {
        return userService.create(userDto).getEmail();
    }

    @GetMapping("/info")
    @Operation
    public UserInfoDto getUserInfo() {
        return this.getUserInfoDto(this.userService.getUserInfo());
    }

    @PutMapping("/password")
    public boolean setPassword(@Valid @RequestParam String oldPassword, @Valid @RequestParam String newPassword) {
        return userService.setUserPassword(oldPassword, newPassword);
    }

    @PutMapping("/info")
    public UserInfoDto setUserInfo(@Valid @RequestBody User userInfoDto) {
        User user = this.userService.setUserInfo(userInfoDto);
        return this.getUserInfoDto(user);
    }

    UserInfoDto getUserInfoDto(User user) {
        if (user == null) return null;
        return this.modelMapper.map(user,
                UserType.SCIENTIST.equals(user.getUserType()) ? ScientistInfoDto.class : TeacherInfoDto.class
        );
    }
}
