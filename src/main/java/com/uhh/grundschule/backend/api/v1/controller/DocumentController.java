package com.uhh.grundschule.backend.api.v1.controller;

import com.uhh.grundschule.backend.api.v1.dto.document.DocumentDto;
import com.uhh.grundschule.backend.api.v1.dto.document.MaterialDto;
import com.uhh.grundschule.backend.business.service.api.DocumentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/document")
@AllArgsConstructor(onConstructor_ = @Autowired)
@ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "403", description = "User not authenticated", content = @Content(schema = @Schema())),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema()))
})
@Tag(name = "Document", description = "Endpoint for retrieving, modifying and creating progress documents.")
public class DocumentController {

    private final DocumentService documentService;
    private final ModelMapper modelMapper;

    /**
     * HTTP POST = Create new document
     *
     * @param document A Progress document DTO containing data.
     *
     */
    @PostMapping()
    @Operation(summary = "Creates a Progress document.", description = "Creates a new Progress document and returns the ID of the created document. All parameters can be set in the http body.")
    @ApiResponse(responseCode = "200", description = "The Progress document was successfully created and the response body contains the ID of the created document.")
    @ApiResponse(responseCode = "400", description = "Invalid Progress document supplied.", content = @Content)
    public Long create(@Valid @RequestBody DocumentDto document) {
        return documentService.create(document);
    }

    /**
     * HTTP PUT for ID = Replace all fields
     *
     * @param documentId A document ID
     * @param document A complete Progress document DTO
     */
    @PutMapping("/{documentId}")
    @Operation(summary = "Overwrites a Progress document.", description = "Overwrites a Progress document. Id etc. can not be overwritten.")
    @ApiResponse(responseCode = "200", description = "The Progress document was successfully overwritten.")
    @ApiResponse(responseCode = "400", description = "Invalid Convo Process supplied.", content = @Content)
    @Transactional
    public void edit(@Valid @PathVariable final Long documentId, @Valid @RequestBody DocumentDto document) {
        documentService.overwrite(documentId, document);
    }

    @GetMapping("/{documentId}")
    @Operation(summary = "Finds a Progress a complete document by id.")
    @ApiResponse(responseCode = "200", description = "The Progress document was retrieved successfully.")
    public DocumentDto get(@Valid @PathVariable Long documentId) {
        return this.modelMapper.map(documentService.get(documentId), DocumentDto.class);
    }

    @GetMapping("/{documentId}/info")
    @Operation(summary = "Finds a Progress document meta data only by id.")
    @ApiResponse(responseCode = "200", description = "The Progress document meta data was retrieved successfully.")
    public DocumentDto getInformation(@PathVariable Long documentId) {
        return this.modelMapper.map(documentService.getInformation(documentId), DocumentDto.class);
    }

    @GetMapping("/{documentId}/material")
    @Operation(summary = "Finds a Progress documents material by the document id.")
    @ApiResponse(responseCode = "200", description = "The Progress document material was retrieved successfully.")
    public MaterialDto getMaterial(@PathVariable Long documentId) {
        return this.modelMapper.map(documentService.getMaterialList(documentId), MaterialDto.class);
    }

    /**
     * HTTP GET - document list
     *
     * @return Returns a list of all Progress documents each represented as Json. Without Materials.
     */
    @Operation(summary = "Finds all Progress documents.", description = "Returns a list of all documents each represented as Json. Without Materials.")
    @ApiResponse(responseCode = "200", description = "The Progress documents were retrieved successfully")
    @GetMapping("/list")
    public List<DocumentDto> getList() {
        return documentService.getList().stream()
                .map(doc -> this.modelMapper.map(doc, DocumentDto.class))
                .collect(Collectors.toList());
    }

    /**
     * HTTP GET - document list
     *
     * @return Returns a list of all Progress documents each represented as Json. Without Materials.
     */
    @Operation(summary = "Finds all Progress documents.", description = "Returns a list of all documents each represented as Json. Without Materials.")
    @ApiResponse(responseCode = "200", description = "The Progress documents were retrieved successfully")
    @GetMapping("/list/meta")
    public List<DocumentDto> getMetaDataList() {
        return documentService.getListMetaData().stream()
                .map(documentMetaData -> this.modelMapper.map(documentMetaData, DocumentDto.class))
                .collect(Collectors.toList());
    }

    /**
     * HTTP GET - document list created by user
     *
     * @return Returns a list of all documents created by user, each represented as Json. Without Materials.
     */
    @Operation(summary = "Finds all documents created by user.", description = "Returns a list of all documents created by user, each represented as Json. Without Materials.")
    @ApiResponse(responseCode = "200", description = "The progress documents were retrieved successfully")
    @GetMapping("/list/editable")
    public List<DocumentDto> getListCreatedByUser() {
        return documentService.getListByUser().stream()
                .map(doc -> this.modelMapper.map(doc, DocumentDto.class))
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{documentId}")
    @Operation(summary = "Deletes a Progress Document by id.")
    @ApiResponse(responseCode = "200", description = "The Progress document was deleted successfully.")
    public void delete(@Valid @PathVariable Long documentId) {
        documentService.delete(documentId);
    }

}
