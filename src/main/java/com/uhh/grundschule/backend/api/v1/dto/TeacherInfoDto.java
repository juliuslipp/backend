package com.uhh.grundschule.backend.api.v1.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class TeacherInfoDto extends UserInfoDto {

    private EducationDto education;

}
