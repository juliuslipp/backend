package com.uhh.grundschule.backend.api.v1.dto.document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.uhh.grundschule.backend.common.enums.LearningPhase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeachingPhaseDto {

    private LearningPhase learningPhase;
    private String duration;
    private String description;
    private String tasks;
    private String positiveNotes;
    private String problems;
    private MaterialDto material;
}
