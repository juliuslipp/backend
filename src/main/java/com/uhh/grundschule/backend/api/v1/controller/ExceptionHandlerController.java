package com.uhh.grundschule.backend.api.v1.controller;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.uhh.grundschule.backend.business.exceptions.ConflictException;
import com.uhh.grundschule.backend.business.exceptions.NotFoundException;
import com.uhh.grundschule.backend.business.exceptions.UnprocessableEntityException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.marker.LogstashMarker;
import net.logstash.logback.marker.Markers;
import okhttp3.ResponseBody;
import org.postgresql.util.PSQLException;
import org.slf4j.Marker;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import retrofit2.HttpException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Exception-Handler for the REST-API.
 * <p>
 * Logs exceptions and turns them into corresponding HTTP codes.
 */
@Slf4j
@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    /**
     * Will be executed when a {@link IllegalArgumentException} occurs.
     *
     * @param e caught exception
     * @return Empty response with 400.
     */
    @ExceptionHandler(IllegalArgumentException.class)
    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content)
    public ResponseEntity<Object> handleIllegalArgumentException(final IllegalArgumentException e) {

        final Marker marker = this.getBaseExceptionMarker(e);

        log.warn(marker, "IllegalArgumentException occurred", e);

        return ResponseEntity.badRequest().build();
    }

    /**
     * Will be executed when a {@link NotFoundException} occurs.
     * <p>
     * This exception will log some basic-information
     * of {@link NotFoundException}.
     *
     * @param e caught exception
     * @return Empty response with 404-code
     */
    @ExceptionHandler(NotFoundException.class)
    @ApiResponse(responseCode = "404", description = "Not found", content = @Content)
    public ResponseEntity<Object> handleNotFoundException(final NotFoundException e) {
        log.info("{} with ID {} was not found.", e.getType(), e.getSearchTerm());
        return ResponseEntity.notFound().build();
    }

    /**
     * Will be executed when a {@link AccessDeniedException} occurs.
     *
     * @param e caught exception
     * @return Empty response with 403.
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ApiResponse(responseCode = "403", description = "Access denied", content = @Content)
    public ResponseEntity<Object> handleAccessDeniedException(final AccessDeniedException e) {
        final Marker marker = this.getBaseExceptionMarker(e);
        log.warn(marker, "AccessDeniedException occurred", e);

        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Will be executed when a {@link AuthenticationException} occurs (Spring exception, thrown when an unauthenticated client
     * tries to access protected endpoints).
     * <p>
     * This will log the ip-address (attack?).
     *
     * @param e       caught exception
     * @param request Http-request with inner information
     * @return Empty response with 403.
     */
    @ExceptionHandler(AuthenticationException.class)
    @ApiResponse(responseCode = "403", description = "Access denied for unknown user", content = @Content)
    public ResponseEntity<Object> handleAuthenticationException(final AuthenticationException e, final HttpServletRequest request) {
        final var marker = this.getBaseExceptionMarker(e)
                .and(Markers.append("userIp", request.getRemoteAddr()))
                .and(Markers.append("requested", request.getRequestURI()));

        log.warn(marker, "Unknown user tried to access REST", e);

        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Will be executed when a {@link ConflictException} occurs.
     *
     * @param e caught exception
     * @return Response with 409.
     */
    @ExceptionHandler(ConflictException.class)
    @ApiResponse(responseCode = "409", description = "Conflict", content = @Content)
    public ResponseEntity<Object> handleConflictException(final ConflictException e) {
        final Marker marker = this.getBaseExceptionMarker(e);
        log.warn(marker, "ConflictException occurred", e);

        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }

    /**
     * Will be executed when a {@link UnprocessableEntityException} occurs.
     *
     * @param e caught exception
     * @return Response with 422.
     */
    @ExceptionHandler(UnprocessableEntityException.class)
    @ApiResponse(responseCode = "422", description = "Unprocessable Entity", content = @Content)
    public ResponseEntity<Object> handleUnprocessableEntityException(final UnprocessableEntityException e) {
        final Marker marker = this.getBaseExceptionMarker(e);

        log.warn(marker, "UnprocessableEntityException occurred", e);

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(e.getMessage());
    }

    /**
     * Will be executed when a {@link DataIntegrityViolationException} occurs.
     *
     * @param e caught exception
     * @return Response with 409.
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    @ApiResponse(responseCode = "409", description = "Unprocessable Entity", content = @Content)
    public ResponseEntity<Object> handleDataIntegrityViolationException(final DataIntegrityViolationException e) {
        final Marker marker = this.getBaseExceptionMarker(e);
        log.warn(marker, "DataIntegrityViolationException occurred", e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getCause().getMessage());
    }

    /**
     * Will be executed when a {@link PSQLException} occurs.
     *
     * @param e caught exception
     * @return Response with 422.
     */
    @ExceptionHandler(PSQLException.class)
    @ApiResponse(responseCode = "422", description = "Unprocessable Entity", content = @Content)
    public ResponseEntity<Object> handleUnprocessableEntityException(final PSQLException e) {
        final Marker marker = this.getBaseExceptionMarker(e);
        log.warn(marker, "PSQLException occurred", e);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(e.getMessage());
    }

    /**
     * Will be executed when a unexpected exception occurs.
     * <p>
     * This will log some details to find the error.
     *
     * @param e caught Exception
     * @return Response with error-details and 500-code.
     */
    @ExceptionHandler(Exception.class)
    @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    public ResponseEntity<Object> handleUnexpectedExceptions(final Exception e) {

        final ZonedDateTime currentTime = ZonedDateTime.now();
        final LogstashMarker marker = this.getBaseExceptionMarker(currentTime, e);

        log.error(marker, "unexpected exception occurred", e);

        //ugly but the only way to get Data Service error body
        if (e instanceof RuntimeException
                && e.getCause() instanceof ExecutionException
                && e.getCause().getCause() instanceof HttpException) {
            this.printErrorBody((HttpException) e.getCause().getCause());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(currentTime);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status,
                                                               final WebRequest request) {
        //Get all errors
        final List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> error.getField() + ": " + error.getDefaultMessage())
                .collect(Collectors.toList());

        for (final String logMessage : errors) {
            log.debug(logMessage);
        }
        return ResponseEntity.badRequest().build();
    }


    /**
     * Prints the {@link HttpException#response()}s error body
     *
     * @param httpException exception to be logged
     */
    private void printErrorBody(final HttpException httpException) {
        String errorBody = "";
        if (httpException.response() != null) {
            final ResponseBody responseBody = httpException.response().errorBody();
            if (responseBody != null) {
                try {
                    errorBody = responseBody.string();
                } catch (final IOException ignored) {
                }
            }
        }
        log.error("HTTP error body: {}", errorBody);
    }

    private LogstashMarker getBaseExceptionMarker(final ZonedDateTime currentTime, final Exception e) {
        return Markers.append("exception", e.getClass().getSimpleName())
                .and(Markers.append("errorTimeStamp", currentTime.toString()));
    }

    private LogstashMarker getBaseExceptionMarker(final Exception e) {
        return this.getBaseExceptionMarker(ZonedDateTime.now(), e);
    }

}
