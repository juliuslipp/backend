package com.uhh.grundschule.backend.api.v1.dto.document;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentDto {

    private Long id;
    private Date startTime;
    private Date endTime;
    private Date date;
    private String topic;
    private String textbook;
    private String state;
    private String subject;
    private String schoolClass;
    private String school;

    private List<TeachingPhaseDto> teachingPhaseList;
    private List<MaterialDto> materialList;
    private List<CommentDto> commentList;
}
