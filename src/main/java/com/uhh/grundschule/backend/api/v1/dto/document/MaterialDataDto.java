package com.uhh.grundschule.backend.api.v1.dto.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Base64;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MaterialDataDto {

    private Byte[] data;
    private String fileType;
    private String fileName;

    public String getData() {
        byte[] bytes = new byte[this.data.length];
        int i = 0;
        for (Byte b : this.data) bytes[i++] = b;
        byte[] encoded = Base64.getEncoder().encode(bytes);
        return new String(encoded);
    }

    @JsonIgnore
    public Byte[] getByteData() {
        return this.data;
    }
}
