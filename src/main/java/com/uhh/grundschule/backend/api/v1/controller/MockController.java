package com.uhh.grundschule.backend.api.v1.controller;

import com.uhh.grundschule.backend.api.v1.dto.AddressDto;
import com.uhh.grundschule.backend.api.v1.dto.UserInfoDto;
import com.uhh.grundschule.backend.api.v1.dto.document.DocumentDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@RestController
@RequestMapping("/api/v1")
public class MockController {

    private final UserInfoDto mockUserInfo;
    private final DocumentDto mockDocument;

    public MockController() {
        this.mockUserInfo = getMockUserInfo();
        this.mockDocument = getMockDocument();
    }

    private DocumentDto getMockDocument() {
        DocumentDto document = new DocumentDto();
        document.setDate(new Date(System.currentTimeMillis()));
        document.setSchool("Musterschuhle");
        document.setSchoolClass("Musterklasse");
        document.setState("Berlin");
        document.setSubject("Mathe");
        document.setTextbook("Musterbuch");
        document.setTopic("LinA");

        return document;
    }

    private UserInfoDto getMockUserInfo() {
        UserInfoDto userInfo = new UserInfoDto();
        userInfo.setAddress(
                new AddressDto("Berlin", "Musterstadt", "69696", "Musterstraße 35", "")
        );
        userInfo.setBirthDate(
                Date.from(LocalDate.parse("2000-02-22").atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
        );
        userInfo.setEmail("max@mustermann.de");
        userInfo.setFirstName("Max");
        userInfo.setLastName("Mustermann");
        userInfo.setPhoneNumber("015169696969");
        return userInfo;
    }

    @GetMapping("/user/info/mock")
    public UserInfoDto mockUserInfo() {
        return this.mockUserInfo;
    }

    @GetMapping("document/mock")
    public DocumentDto mockDocument() {
        return this.mockDocument;
    }

}
