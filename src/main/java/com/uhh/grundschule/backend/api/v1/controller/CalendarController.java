package com.uhh.grundschule.backend.api.v1.controller;

import com.uhh.grundschule.backend.api.v1.dto.CalendarDto;
import com.uhh.grundschule.backend.business.service.api.CalendarService;
import com.uhh.grundschule.backend.common.model.Calendar;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/calendar")
@AllArgsConstructor(onConstructor_ = @Autowired)
@ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "403", description = "User not authenticated", content = @Content(schema = @Schema())),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema()))
})
@Tag(name = "Calendar", description = "Endpoint for retrieving, modifying and creating the users calendar.")
public class CalendarController {

    private final CalendarService calendarService;
    private final ModelMapper modelMapper;

    @PutMapping("/appointments/reset")
    @Operation(summary = "Resets calendar appointments")
    @ApiResponse(responseCode = "200", description = "The Progress calendar was reset successfully.")
    public void resetAppointments() {
        this.calendarService.reset();
    }

    @GetMapping
    @Operation(summary = "Gets calendar")
    @ApiResponse(responseCode = "200", description = "The Calendar was fetched successfully.")
    public CalendarDto get() {
        return this.modelMapper.map(this.calendarService.get(), CalendarDto.class);
    }

    @PutMapping("/appointments")
    @Operation(summary = "Set appointments")
    @ApiResponse(responseCode = "200", description = "The calendar appointments were set successfully.")
    public void setAppointments(@Valid @RequestBody CalendarDto calendarDto) {
        this.calendarService.setAppointments(calendarDto.getAppointmentData());
    }

    @PutMapping
    @Operation(summary = "Set calendar meta data")
    @ApiResponse(responseCode = "200", description = "The calendar meta data was set successfully.")
    public void set(@Valid @RequestBody CalendarDto calendarDao) {
        this.calendarService.set(
                this.modelMapper.map(calendarDao, Calendar.class)
        );
    }


}
