package com.uhh.grundschule.backend.api.v1.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.uhh.grundschule.backend.common.model.teaching.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class EducationDto {

    private Teacher teacher;

    private String facility;

    private String title;

    private String degree;

    private Date startDate;

    private Date endDate;

}
