package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.teaching.document.TeachingPhase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeachingPhaseDao extends JpaRepository<TeachingPhase, Long> {
}
