package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.teaching.document.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentDao extends JpaRepository<Comment, Long> {
}
