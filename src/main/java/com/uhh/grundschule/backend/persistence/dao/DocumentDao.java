package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.teaching.document.Document;
import com.uhh.grundschule.backend.common.model.teaching.document.DocumentMetaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DocumentDao extends JpaRepository<Document, Long> {

    @Query("select new com.uhh.grundschule.backend.common.model.teaching.document.DocumentMetaData(d.startTime, d.endTime, d.date, d.topic, d.textbook, d.state, d.subject, d.schoolClass, d.school, d.id) from Document d")
    List<DocumentMetaData> findAllMetaData();

}
