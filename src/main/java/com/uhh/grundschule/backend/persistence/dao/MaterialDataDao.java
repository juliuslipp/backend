package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.teaching.document.MaterialData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialDataDao extends JpaRepository<MaterialData, Long> {
}
