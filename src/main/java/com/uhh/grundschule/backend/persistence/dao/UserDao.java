package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserDao extends JpaRepository<User, Long> {

  User findByEmail(String email);

  @Modifying
  @Query("update User  u  set u.password = :pw where u.id = :id")
  int setPassword(@Param(value = "id") long id, @Param(value = "pw") String pw);
}
