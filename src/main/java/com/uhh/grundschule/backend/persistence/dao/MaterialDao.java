package com.uhh.grundschule.backend.persistence.dao;

import com.uhh.grundschule.backend.common.model.teaching.document.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialDao extends JpaRepository<Material, Long> {
}
