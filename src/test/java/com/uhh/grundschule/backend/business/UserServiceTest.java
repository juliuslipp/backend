package com.uhh.grundschule.backend.business;

import com.uhh.grundschule.backend.api.v1.dto.UserDto;
import com.uhh.grundschule.backend.business.service.api.UserService;
import com.uhh.grundschule.backend.business.service.impl.UserServiceImpl;
import com.uhh.grundschule.backend.common.model.User;
import com.uhh.grundschule.backend.persistence.dao.UserDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Test class for {@link UserService}.
 */
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest {

    @Mock
    private UserDao userDao;

    @Mock
    private BCryptPasswordEncoder encoder;

    private UserService userService;
    private final UserDto testUserDto = new UserDto();

    private static final String TEST_USER_FIRST_NAME = "Julius";
    private static final String TEST_USER_NAME = "Lipp";
    private static final String TEST_USER_PASSWORD = "12345";
    private static final String TEST_USER_EMAIL = "test@test.de";

    @BeforeEach
    void init() {
        this.userService = new UserServiceImpl(encoder, new ModelMapper(), userDao);
        this.testUserDto.setFirstName(TEST_USER_FIRST_NAME);
        this.testUserDto.setLastName(TEST_USER_NAME);
        this.testUserDto.setEmail(TEST_USER_EMAIL);
        this.testUserDto.setPassword(TEST_USER_PASSWORD);
    }

    /**
     * Test method for {@link UserServiceImpl#create}.
     * case: create Convo Process Definition is successful
     */
    @Test
    void testCreateSuccessful() {
        // ARRANGE
        final User testUser = new User();
        testUser.setEmail(TEST_USER_EMAIL);
        testUser.setFirstName(TEST_USER_FIRST_NAME);
        testUser.setLastName(TEST_USER_NAME);


        when(this.userDao.save(any(User.class)))
                .thenReturn(testUser);

        // ACT
        this.userService.create(this.testUserDto);

        // ASSERT
        final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        Mockito.verify(this.userDao, Mockito.times(1)).save(captor.capture());
        final User actual = captor.getValue();

        assertEquals(TEST_USER_FIRST_NAME, actual.getFirstName());
        assertEquals(TEST_USER_NAME, actual.getLastName());
        assertEquals(TEST_USER_EMAIL, actual.getEmail());
    }
}
